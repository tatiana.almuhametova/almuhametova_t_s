#include <opencv2/opencv.hpp>
#include <string>
#include <iostream>
#include <filesystem>
#include <clocale>
using namespace cv;
using namespace std;

struct Blob {
    float confidence;
    cv::Point2d location;
    float radius;
};

void filterBlobsInImage (Mat& binaryImage, vector<Blob>& blobs, vector<std::vector<cv::Point>>&curContours, SimpleBlobDetector::Params& params)
{
    vector<vector<Point>> contours;
    //находим контуры
    findContours(binaryImage, contours, RETR_LIST, CHAIN_APPROX_NONE); 

    //фильтрация контуров по параметрам
    for (size_t contourIdx = 0; contourIdx < contours.size(); contourIdx++) {
        Blob blob;
        blob.confidence = 1;

    Moments moms = moments(Mat(contours[contourIdx]));
        if (params.filterByArea)
        {
            double area = moms.m00;
            if (area < params.minArea || area >= params.maxArea)
                continue;
        }

        if (params.filterByCircularity)
        {
            double area = moms.m00;
            double perimeter = arcLength(Mat(contours[contourIdx]), true);
            double ratio = 4 * CV_PI * area / (perimeter * perimeter);
            if (ratio < params.minCircularity || ratio >= params.maxCircularity)
                continue;
        }

        if (params.filterByInertia)
        {
            double denominator = sqrt(pow(2 * moms.mu11, 2) + pow(moms.mu20 - moms.mu02, 2));
            const double eps = 1e-2;
            double ratio;
            if (denominator > eps)
            {
                double cosmin = (moms.mu20 - moms.mu02) / denominator;
                double sinmin = 2 * moms.mu11 / denominator;
                double cosmax = -cosmin;
                double sinmax = -sinmin;

                double imin = 0.5 * (moms.mu20 + moms.mu02) - 0.5 * (moms.mu20 - moms.mu02) * cosmin - moms.mu11 * sinmin;
                double imax = 0.5 * (moms.mu20 + moms.mu02) - 0.5 * (moms.mu20 - moms.mu02) * cosmax - moms.mu11 * sinmax;
                ratio = imin / imax;
            }
            else
            {
                ratio = 1;
            }

            if (ratio < params.minInertiaRatio || ratio >= params.maxInertiaRatio)
                continue;

            blob.confidence = ratio * ratio;
        }

        if (params.filterByConvexity)
        {
            vector < Point > hull;
            convexHull(Mat(contours[contourIdx]), hull);
            double area = contourArea(Mat(contours[contourIdx]));
            double hullArea = contourArea(Mat(hull));
            double ratio = area / hullArea;
            if (ratio < params.minConvexity || ratio >= params.maxConvexity)
                continue;
        }
        // Вычисляем свойства входящей в контур группы пикселей
        blob.location = Point2d(moms.m10 / moms.m00, moms.m01 / moms.m00);
        vector<double> dists;
        for (size_t pointIdx = 0; pointIdx < contours[contourIdx].size(); pointIdx++) {
            Point2d pt = contours[contourIdx][pointIdx];
            dists.push_back(norm(blob.location - pt));
        }
        sort(dists.begin(), dists.end());
        blob.radius = (dists[(dists.size() - 1) / 2] + dists[dists.size() / 2]) / 2.;

        blobs.push_back(blob);
        curContours.push_back(contours[contourIdx]);
    }
}

void  detectFlareMask(string& path_to_image, SimpleBlobDetector::Params& params){
    Mat mask;
    vector<vector<Blob>> blobs;
    vector<vector<cv::Point>> contours;
    
    Mat image = imread(path_to_image, 1);
  
    cv::Mat imageGray = image;

    if (image.channels() != 1) {
        cv::cvtColor(image, imageGray, COLOR_BGR2GRAY);
    }

    // Множественная бинаризация
    for (double thresh = params.minThreshold; thresh < params.maxThreshold; thresh += params.thresholdStep) {
        Mat binarizedImage;
        threshold(imageGray, binarizedImage, thresh, 255, cv::THRESH_BINARY);

        // Фильтруем группы пикселей по параметрам
        vector<Blob> curBlobs;
        vector<vector<Point>> curContours, newContours;
        filterBlobsInImage ( binarizedImage , curBlobs ,curContours, params) ;

        // Обединяем контуры
        vector<vector<Blob>> newBlobs;

        for (size_t i = 0; i < curBlobs.size(); i++) {
            bool isNew = true;
            for (size_t j = 0; j < blobs.size(); j++) {
                double dist = norm(blobs[j][blobs[j].size() / 2].location - curBlobs[i].location);
                isNew = dist >= params.minDistBetweenBlobs && dist >= blobs[j][blobs[j].size() / 2].radius && dist >= curBlobs[i].radius;
                if (!isNew) {
                    blobs[j].push_back(curBlobs[i]);

                    size_t k = blobs[j].size() - 1;
                    while (k > 0 && blobs[j][k].radius < blobs[j][k - 1].radius) {
                        blobs[j][k] = blobs[j][k - 1];
                        k--;
                    }
                    blobs[j][k] = curBlobs[i];

                    break;
                }
            }
            if (isNew) {
                newBlobs.push_back(std::vector<Blob>(1, curBlobs[i]));
                newContours.push_back(curContours[i]);
            }
        }

        copy(newBlobs.begin(), newBlobs.end(), std::back_inserter(blobs));
        copy(newContours.begin(), newContours.end(), std::back_inserter(contours));
    }

    //формируем итоговую маску
    mask = Mat::zeros(imageGray.size(), CV_8UC1);
    drawContours(mask, contours, -1, 255, -1, 8);
    imwrite(path_to_image+"FlareDetector.jpg", mask);
    
}

int main()
{ 
    SimpleBlobDetector::Params params;
    params.minThreshold = 235;
    params.maxThreshold = 255;
    params.thresholdStep = 5;
    params.minDistBetweenBlobs = 50;

    params.filterByCircularity = true;
    params.minCircularity = 0.4;
    params.maxCircularity = 1;

    params.filterByArea = false;
    params.minArea = 400;
    params.maxArea = 1500;

    params.filterByConvexity = true;
    params.minConvexity = 0.8;
    params.maxConvexity = 1;

    params.filterByInertia = true;
    params.minInertiaRatio = 0.7;
    params.maxInertiaRatio = 1;


    setlocale(LC_CTYPE, "rus");
    cout << "Введите путь к директории с изображениями в формате C:/FlareDetector/*.jpg" << endl;
    string path;
    cin >> path;
    vector<string> image_name; 
    glob(path, image_name, false);

    for (int i= 0; i < image_name.size(); i++)
    { 
        detectFlareMask(image_name[i], params);
    }

	waitKey(0);
	return 0;
}