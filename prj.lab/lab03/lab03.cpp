#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;
int my_function(int x) {
	if (x <= 50)
	{
		return(5 * x);
	}
	if ((x > 50)&&(x<100))
	{
		return(-5*(x - 50) + 250);
	}
	if (x>=100)
	{
		return(x-100);
	}
}

Mat Histogram(const Mat& img, float y_scale, int hist_high = 300)
{
	Mat grayImg = img;
	array<int, 256> hist = {};
	for (int j_row = 0; j_row < grayImg.rows; j_row++) {
		for (int i_col = 0; i_col < grayImg.cols; i_col++) {
			hist[grayImg.at<uint8_t>(j_row, i_col)] += 1; //������� ���-�� �������� ��� ������� �������
		}
	}
	Mat histogram(hist_high, 256, CV_8UC1, Scalar(255, 0, 0));
	for (int x = 0; x < 256; x++) {
		line(histogram, Point(x, 0), Point(x, hist_high - y_scale * hist[x]), Scalar(10, 255, 255), 1);
	}
	return histogram;
}

//���������� ����������� � ������� ������ ����
Mat Global_binarization(const Mat& GrayImg)
{
	Mat BinImg = GrayImg.clone();

	//������� �����������
	array<int, 256> hist = {};
	for (int j_row = 0; j_row < BinImg.rows; j_row++) {
		for (int i_col = 0; i_col < BinImg.cols; i_col++) {
			hist[BinImg.at<uint8_t>(j_row, i_col)] += 1; //������� ���-�� �������� ��� ������� �������
		}
	}

	int N = BinImg.cols * BinImg.rows;
	double* p = new double[256];

	for (int i = 0; i < 256; i++)
	{
		p[i] = (double)hist[i] / (double)N;
	}

	//��������� ����� �����������
	double M1 = 0;
	double M2 = 0;

	for (int i = 0; i < 256; i++)
	{
		M1 = M1 + hist[i] * p[i];
		M2 = M2 + hist[i] * hist[i] * p[i];
	}

	double sigma_general = M2 - (M1 * M1);
	double nu = 0;
	int max = 0;

	for (int k = 1; k < 255; k++)
	{
		double w0 = 0;
		for (int i = 0; i < k; i++)
		{
			w0 = w0 + p[i];
		}
		double w1 = 1 - w0;
		double mu0 = 0;
		for (int i = 0; i < k; i++)
		{
			mu0 = mu0 + (i * p[i] / w0);
		}
		double mu1 = 0;
		for (int i = k; i < 256; i++)
		{
			mu1 = mu1 + (i * p[i] / w1);
		}
		double sigma_class = w0 * w1 * (mu1 - mu0) * (mu1 - mu0);
		if (sigma_class / sigma_general > nu)
		{
			nu = sigma_class / sigma_general;
			max = k;
		}
	}
	//����������� �� ������
	for (int i = 0; i < BinImg.cols; i++)
	{
		for (int j = 0; j < BinImg.rows; j++)
		{
			if (BinImg.at<uint8_t>(j, i) < max)
			{
				BinImg.at<uint8_t>(j, i) = 0;
			}
			else
			{
				BinImg.at<uint8_t>(j, i) = 255;
			}
		}
	}

	return BinImg;
}

//��������� ����������� � ������� ������ �������
Mat Local_binarization(const Mat& GrayImg)
{
	Mat BinImg;
	GrayImg.convertTo(BinImg, CV_32F, 1 / 255.0);

	Mat img_sqr, mx2;
	pow(BinImg, 2, img_sqr);
	int q = 16;
	boxFilter(img_sqr, mx2, -1, Size(q, q), Point(1, -1), true, BORDER_DEFAULT);

	Mat avg, m2x;
	boxFilter(BinImg, avg, -1, Size(q, q), Point(1, -1), true, BORDER_DEFAULT);
	pow(avg, 2, m2x);

	Mat dx = mx2 - m2x;
	Mat stddev(BinImg.rows, BinImg.cols, CV_32FC1);

	sqrt(dx, stddev);

	double k = -0.2;
	Mat threshold = avg + k * stddev;
	Mat niblack(BinImg.rows, BinImg.cols, CV_32FC1);

	for (int i = 0; i < BinImg.rows; i += 1)
	{
		for (int j = 0; j < BinImg.cols; j += 1)
		{
			if (BinImg.at<float>(i, j) > threshold.at<float>(i, j))
				niblack.at<float>(i, j) = 255;
			else niblack.at<float>(i, j) = 0;
		}
	}
	return niblack;
}
int main()
{
    //1. ��������� � �������� ��������� ����������� $S$ � ��������� ������.
    Mat img = imread("cross_0256x0256.png");
    Mat grayImg;
    cvtColor(img, grayImg, COLOR_BGR2GRAY);
    imwrite("lab03.src.png", grayImg);

    //2. ��������� � ���������� ����������� $H_s$ ������������� ������� �������� ��������� �����������.
	int hist_high = 300; //������ �����������
	int max = 0;//������������ �������� � �����������
	array<int, 256> hist = {};
	for (int j_row = 0; j_row < grayImg.rows; j_row++) {
		for (int i_col = 0; i_col < grayImg.cols; i_col++) {
			hist[grayImg.at<uint8_t>(j_row, i_col)] += 1; //������� ���-�� �������� ��� ������� �������
		}
	}
	for (int i = 0; i < 256; i++)
	{
		if (max < hist[i])
		{
			max = hist[i];
		}
	}
	float y_scale = (1.0 * hist_high / max);

	Mat histogram = Histogram(grayImg, y_scale, hist_high);
	imshow("Histogram of image", histogram);
	imwrite("lab03.hist.src.png", histogram);

	//3. ������������� ��������� ������� �������������� �������.��������� ������ $V$ ��������� ������� �������������� �������.

	Mat brightness(256, 256, CV_8UC1, Scalar(255, 0, 0));
	for (int x = 1; x < brightness.cols; x++) {
		line(brightness, Point(x-1, 255-my_function(x - 1)), Point(x,255- my_function(x)),(1));
	}
	imwrite("lab03.lut.png", brightness);

	//4. ��������� ��������� ������� �������������� ������� � ��������� ����������� � �������� $L$, ���������� ����������� $H_L$ ���������������� �����������.
	Mat image_with_function;
	grayImg.copyTo(image_with_function);
	for (int j_row = 0; j_row < grayImg.rows; j_row++) {
		for (int i_col = 0; i_col < grayImg.cols; i_col++) {
			image_with_function.at<uint8_t>(j_row, i_col) = my_function(image_with_function.at<uint8_t>(j_row, i_col));
		}
	}
	imshow("IMG brigtness function", image_with_function);
	imwrite("lab03.lut.src.png", image_with_function);
	
	Mat histogram_with_function = Histogram(image_with_function, y_scale, hist_high);
	imshow("Histogram of image with fuction", histogram_with_function);
	imwrite("lab03.hist.lut.src.png", histogram_with_function);

	//5. ��������� CLAHE � ����� ������� �������� ����������(��������������� ������������ ����������� $C_i$ � �� �����������
	Mat img_clahe1;
	createCLAHE(15, Size(15, 15))->apply(grayImg, img_clahe1);
	imshow("clahe 1", img_clahe1);
	imwrite("lab03.clahe.1.png", img_clahe1);
	Mat histogram_clahe1 = Histogram(img_clahe1, y_scale, hist_high);
	imshow("Hist clahe1", histogram_clahe1);
	imwrite("lab03.hist.clahe.1.png", histogram_clahe1);

	Mat img_clahe2;
	createCLAHE(10, Size(10, 10))->apply(grayImg, img_clahe2);
	imshow("clahe 2", img_clahe2);
	imwrite("lab03.clahe.2.png", img_clahe2);
	Mat histogram_clahe2 = Histogram(img_clahe2, y_scale, hist_high);
	imshow("Hist clahe2", histogram_clahe2);
	imwrite("lab03.hist.clahe.2.png", histogram_clahe2);

	Mat img_clahe3;
	createCLAHE(20, Size(5,5))->apply(grayImg, img_clahe3);
	imshow("clahe 3", img_clahe3);
	imwrite("lab03.clahe.3.png", img_clahe3);
	Mat histogram_clahe3 = Histogram(img_clahe3, y_scale, hist_high);
	imshow("Hist clahe3", histogram_clahe3);
	imwrite("lab03.hist.clahe.3.png", histogram_clahe3);

	//6. ����������� ���������� ����� �����������(��������� ����� �� �����������, ��������� ��������� �����������).��������������� �� ����� ����������� �������� $S$ � �������������� $B_G$ �����������.
	
	Mat global_binarization_image;
	global_binarization_image = Global_binarization(grayImg);
	imshow("global binarization", global_binarization_image);

	Mat Bin_Result = Mat::zeros(grayImg.rows, grayImg.cols *2, CV_8UC1);
	Rect roi1 = Rect(0, 0, grayImg.cols, grayImg.rows);
	Rect roi2 = Rect(img.cols, 0, img.cols, img.rows);
	grayImg.copyTo(Bin_Result(roi1));
	global_binarization_image.copyTo(Bin_Result(roi2));
	imshow("Bin_Result", Bin_Result);
	imwrite("lab03.bin.global.png", Bin_Result);

	//7. ����������� ����� ��������� �����������.��������������� �� ����� ����������� �������� $S$ � �������������� $B_L$ �����������.

	Mat local_binarization_image;
	local_binarization_image = Local_binarization(grayImg);
	imshow("local binarization", local_binarization_image);

	Mat Loc_Result = Mat::zeros(grayImg.rows, grayImg.cols * 2, CV_8UC1);
	Rect roi3 = Rect(0, 0, grayImg.cols, grayImg.rows);
	Rect roi4 = Rect(img.cols, 0, img.cols, img.rows);
	grayImg.copyTo(Loc_Result(roi3));
	local_binarization_image.copyTo(Loc_Result(roi4));
	imshow("Loc_Result", Loc_Result);
	imwrite("lab03.bin.local.png", Loc_Result);
	
	//8. �������� ���� �� ����������� ����� ���������� ��������������� ��������.��������������� �� ����� ����������� �������� ����������� �� � ����� ���������� $M$.
	Mat morph;
	morphologyEx(global_binarization_image, morph, MORPH_OPEN, morphologyDefaultBorderValue());
	
	Mat Morph_Result = Mat::zeros(morph.rows, morph.cols * 2, CV_8UC1);
	Rect roi5 = Rect(0, 0, morph.cols, morph.rows);
	Rect roi6 = Rect(morph.cols, 0, morph.cols, morph.rows);
	global_binarization_image.copyTo(Morph_Result(roi5));
	morph.copyTo(Morph_Result(roi6));
	imshow("Morph_Result", Morph_Result);
	imwrite("lab03.morph.png", Morph_Result);

	//9. ������� ������������ $K$ �������� ����� ����� ��������������� �������� ������ ��������� �����������(����� ������ ��������� ������ � ����� - ��������).
	imwrite("morph.png", morph);
	imwrite("grayImg.png", grayImg);
	Mat alpha_blend, src1, src2, dst;
	src1 = imread("morph.png");
	src2 = imread("grayImg.png");
	double alpha = 0.5; double beta;
	beta = (1.0 - alpha);
	addWeighted(src1, alpha, src2, beta, 0.0, alpha_blend);
	imshow("alpha blending", alpha_blend);
	imwrite("lab03.mask.png", alpha_blend);

	waitKey(0);
	return 0;
}
