## Работа 1. Исследование гамма-коррекции
автор: Альмухаметова Татьяна Сергеевна.
дата: 21.04.2020

<!-- url: https://gitlab.com/tatiana.almuhametova/almuhametova_t_s/prj.lab/lab01 -->

### Задание
1. Сгенерировать серое тестовое изображение $I_1$ в виде прямоугольника размером 768х60 пикселя с плавным изменение пикселей от черного к белому, одна градация серого занимает 3 пикселя по горизонтали.
2. Применить  к изображению $I_1$ гамма-коррекцию с коэффициентом из интервала 2.2-2.4 и получить изображение $G_1$.
3. Сгенерировать серое тестовое изображение $I_2$ в виде прямоугольника размером 768х60 пикселя со ступенчатым изменением яркости от черного к белому (от уровня 5 с шагом 10), одна градация серого занимает 30 пикселя по горизонтали.
4. Применить  к изображению $I_2$ гамма-коррекцию с коэффициентом из интервала 2.2-2.4 и получить изображение $G_2$.
5. Показать визуализацию результатов в виде одного изображения, об

### Результаты

![](lab01.png)
Рис. 1. Результаты работы программы 

### Текст программы

```cpp
#include <opencv2/opencv.hpp>

using namespace cv;

int main()
{
	Mat img1(Mat::zeros(120, 768, CV_8UC1));
	int pix = 0;
	for (int i = 0; i < 60; i++)
		for (int j = 0; j < img1.cols; j = j + 3)
		{
			img1.at<uint8_t>(i, j) = pix;
			img1.at<uint8_t>(i, j + 1) = pix;
			img1.at<uint8_t>(i, j + 2) = pix;
			pix++;
		}
	Mat img2(Mat::zeros(120, 768, CV_8UC1));
	img1.convertTo(img2, CV_32FC1, 1.0 / 255);
	for (int i = 60; i < 120; i++)
		for (int j = 0; j < img2.cols; j++)
		{
			img2.at<float_t>(i, j) = pow(img2.at<float_t>(i - 60, j), 2.2);
		}
	img2.convertTo(img1, CV_8UC1, 255);
	
	Mat img3(Mat::zeros(120, 768, CV_8UC1));
	pix = 5;
	for (int i = 0; i < 60; i++)
	{
		for (int j = 0; j < img3.cols; j = j + 30)
		{
			for (int jf = 0; jf < 30; jf++)
				if (jf + j < img3.cols)
					img3.at<uint8_t>(i, j + jf) = pix;
			pix = pix + 10;
		}
		pix = 0;
	}
	Mat img4(Mat::zeros(120, 768, CV_8UC1));
	img3.convertTo(img4, CV_32FC1, 1.0 / 255);
	
	for (int i = 60; i < 120; i++)
		for (int j = 0; j < img4.cols; j++)
		{
			img4.at<float_t>(i, j) = pow(img4.at<float_t>(i - 60, j), 2.2);
		}
	img4.convertTo(img3, CV_8UC1, 255);
	
	Mat lab01 = Mat::zeros(240, 768, CV_8UC1);
	Mat roi1 = lab01(Rect(0, 0, 768, 120));
	Rect roi2 = Rect(0, 120, 768, 120);
	img1.copyTo(roi1);
	img3.copyTo(lab01(roi2));
	imshow("lab01_Result", lab01);
	imwrite("lab01.png", lab01);
	waitKey(0);
	return 0;
}
```
