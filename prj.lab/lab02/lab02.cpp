#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

int main()
{
    Mat img = imread("cross_0256x0256.png");
    vector<int> quality_params = vector<int>(2);
    String path_95 = "image_95.jpg";
    String path_65 = "image_65.jpg";
    imwrite(path_65, img, { (int)IMWRITE_JPEG_QUALITY, 65 });
    imwrite(path_95, img, { (int)IMWRITE_JPEG_QUALITY, 95 });
    Mat img_jpg65 = imread(path_65);
    Mat img_jpg95 = imread(path_95);

    vector<Mat> rgbChannels_img(3), rgbChannels_jpg65(3), rgbChannels_jpg95(3);
    split(img, rgbChannels_img);
    split(img_jpg65, rgbChannels_jpg65);
    split(img_jpg95, rgbChannels_jpg95);

    Mat b_95;
    Mat b3_95[] = { (rgbChannels_img[0] - rgbChannels_jpg95[0]) * 30, Mat::zeros(img.cols,img.rows, CV_8UC1),Mat::zeros(img.cols,img.rows, CV_8UC1) };
    merge(b3_95, 3, b_95);
    
    Mat g_95;
    Mat g3_95[] = { Mat::zeros(img.cols,img.rows, CV_8UC1), (rgbChannels_img[1] - rgbChannels_jpg95[1]) * 30, Mat::zeros(img.cols,img.rows, CV_8UC1) };
    merge(g3_95, 3, g_95);

    Mat r_95;
    Mat r3_95[] = { Mat::zeros(img.cols,img.rows, CV_8UC1),Mat::zeros(img.cols,img.rows, CV_8UC1),(rgbChannels_img[2] - rgbChannels_jpg95[2]) * 30 };
    merge(r3_95, 3, r_95);

    Mat b_65;   
    Mat b3_65[] = { (rgbChannels_img[0] - rgbChannels_jpg65[0]) * 30, Mat::zeros(img.cols,img.rows, CV_8UC1),Mat::zeros(img.cols,img.rows, CV_8UC1) };
    merge(b3_65, 3, b_65);

    Mat g_65;
    Mat g3_65[] = { Mat::zeros(img.cols,img.rows, CV_8UC1), (rgbChannels_img[1] - rgbChannels_jpg65[1]) * 30, Mat::zeros(img.cols,img.rows, CV_8UC1) };
    merge(g3_65, 3, g_65);

    Mat r_65;
    Mat r3_65[] = { Mat::zeros(img.cols,img.rows, CV_8UC1),Mat::zeros(img.cols,img.rows, CV_8UC1),(rgbChannels_img[2] - rgbChannels_jpg65[2]) * 30 };
    merge(r3_65, 3, r_65);

    Mat grayImg;
    cvtColor(img, grayImg, COLOR_BGR2GRAY);
    Mat grayImg_3;
    Mat Grey3[] = { grayImg,grayImg,grayImg };
    merge(Grey3, 3, grayImg_3);

    Mat grayImg_95;
    cvtColor(img_jpg95, grayImg_95, COLOR_BGR2GRAY);
    Mat grayImg_95_3;
    Mat Grey95_3[] = { grayImg_95,grayImg_95,grayImg_95 };
    merge(Grey95_3, 3, grayImg_95_3);

    Mat grayImg_65;
    cvtColor(img_jpg65, grayImg_65, COLOR_BGR2GRAY);
    Mat grayImg_65_3;
    Mat Grey65_3[] = { grayImg_65,grayImg_65,grayImg_65 };
    merge(Grey65_3, 3, grayImg_65_3);
   
    Mat Result = Mat::zeros(img.rows * 3, img.cols *5, CV_8UC3); 
    Rect roi1 = Rect(0, 0, img.cols, img.rows);
    Rect roi2 = Rect(0, img.rows, img.cols, img.rows);
    Rect roi3 = Rect(img.cols, img.rows, img.cols, img.rows);
    Rect roi4 = Rect(img.cols * 2, img.rows, img.cols, img.rows);
    Rect roi5 = Rect(img.cols * 3, img.rows, img.cols, img.rows);
    Rect roi6 = Rect(img.cols * 4, img.rows, img.cols, img.rows);
    Rect roi7 = Rect(0, img.rows*2, img.cols, img.rows);
    Rect roi8 = Rect(img.cols, img.rows * 2, img.cols, img.rows);
    Rect roi9 = Rect(img.cols * 2, img.rows * 2, img.cols, img.rows);
    Rect roi10 = Rect(img.cols * 3, img.rows * 2, img.cols, img.rows);
    Rect roi11 = Rect(img.cols * 4, img.rows * 2, img.cols, img.rows);
    
    img.copyTo(Result(roi1));
    img_jpg95.copyTo(Result(roi2));
    b_95.copyTo(Result(roi3));
    g_95.copyTo(Result(roi4));
    r_95.copyTo(Result(roi5));
    Mat dif95 = (grayImg_3 - grayImg_95_3)*30;
    dif95.copyTo(Result(roi6));
    img_jpg65.copyTo(Result(roi7));
    b_65.copyTo(Result(roi8));
    g_65.copyTo(Result(roi9));
    r_65.copyTo(Result(roi10));
    Mat dif65 = (grayImg_3 - grayImg_65_3) * 30;
    dif65.copyTo(Result(roi11));

    String path_result = "lab02.png";
    imwrite(path_result, Result);
    imshow("Result", Result);
    waitKey(0);
}
